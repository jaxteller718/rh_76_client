How to install our game options on a server

The rh_gameoptions file should auto generate when you first launch your server. After your server is up check the Main Install folder for Data/Config. You should
see the file located in that folder. You can open like any other txt file and edit it to you liking.

If you do NOT see this file on your server


Either generate a new rh_gameoptions file by starting a single player game or use this default one included.

Tune the settings as you please

When ready upload your rh_gameoptions to your server and place it inside the Data/Config folder

Start up your server

The settings should now save
