# Notes
Zombie appearances, strengths/weaknesses, and whether to keep or not

## Definitely remove

* `zombieGuppyKennethClown` - we don't want takedown notices from the Dead By Daylight developers
* `zombieNurseTemplate` - unnecessary (same as vanilla nurse)
* `zombieSanta` - doesn't match thematically
* `zombieWhiteClown` - doesn't match thematically
* `zombieGuppyBiomechanicalWight` - doesn't match thematically
* `zombieGuppyCreepyCrawly` - doesn't match thematically
* `zombieGuppyInfernalDog` - doesn't match thematically

## Maybe

* `zombieRekt` - blood is way too red (inclined to remove)
* `zombieHugh` - skin is way too blue (inclined to remove)
* `zombieGuppyBaldCop` - albedo is too shiny (inclined to keep)
* `zombieGuppyBeatCop` - too shiny (inclined to keep)
* `zombieGuppyBelle` - too shiny (inclined to keep)
* `zombieGuppyNurse` - kinda haggard, and also a stripper not a nurse (but inclined to keep)
* `zombieGuppyOldManZombie` - skin is too blue
* `zombieGuppyPoliceRalph` - too shiny (inclined to keep)
* `zombieGuppyProstitute` - blood is way too red and she's covered in it (inclined to remove)
* `zombieGuppyMalePatient` - blood is way too red and he's covered in it (inclined to remove)
* `zombieGuppySoldier01*` - weird hands, the guy's face looks like Dracula (inclined to remove)
* `zombieGuppySoldierFemale01*` - weird hands, cartoonish face and hair (inclined to remove)
* `zombieGuppyDoctor*` - all have bright blue scrubs and bright red blood - 04 is especially bad -
  additionally, they all have broken ragdolls
* `zombieMalacayFCiv01` - too shiny (inclined to keep)
* all TSBX except `zombieCopSheriffTSBX*` - most are too clean; look like NPCs not zombies (but inclined to keep)

## Keep

* `zombieFemaleTutorial`
* `zombieBehemoth`
    * but this should *only* spawn during horde night
    * change sounds!
    * AI is super wonky - seems like the fact that it can't fit through 1x2-block openings is messing it up?
    * Too big - melee hand reach is too far (both blocks and humans)
* `zombieGirl`
* `zombieGuppyAbonimation` - this is a "brute" but the brute template is weak (300 health)
  (also it really bugs me that "abomination" is spelled wrong -
  maybe that's intentional, a play on "animation" or something?)
* `zombieGuppyBaldMan`
* `zombieGuppyCarmela`
* `zombieGuppyClot`
* `zombieGuppyHungryJeff`
* `zombieGuppyLucy` - looks a bit like the screamer though - make her one maybe?
* `zombieGuppyPest` - but it should not be a brute - use new crawler sounds
* `zombieGuppyPete`
* `zombieGuppySeth`
* `zombieGuppyAlma` - looks like Wednesday Addams
* `zombieGuppyBlackSoldier*`
* `zombieGuppySoldierLeader`
* `zombieCopSheriffTSBX*`
* `zombieHoly01MUMPFY` - looks like a dead Borderlands bandit, with gas mask on
