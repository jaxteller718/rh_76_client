﻿Key,Source,Context,English
qStarterItems,items,Quest - Note,Starter Kit,,,,,
qStarterItemsDesc,items,Quest - Note,Note from Jax\n\nThe rest of the club is either scattered or dead. You and I are all that is left. I had to head north. I have left you some stuff to help you get to us. Use your special skill wisely because it is all you have in this fucked up world.,,,,,

noteStarterClassSelection,items,Quest - Note,Class Selection,,,,,
noteStarterClassSelectionDesc,items,Quest - Note,Use this paper to select your weapons class. Classes do not lock you out of progressing but they do offer unique recipes for weapons you can craft in each category when you complete the line.,,,,,

noteSecondaryClassSelection,items,Quest - Note,Support Class Selection,,,,,
noteSecondaryClassSelectionDesc,items,Quest - Note,Use this paper to select your support class. Classes do not lock you out of progressing but they do offer unique recipes for your chosen profession you can craft when you complete the line.,,,,,


starterkitName,Quest,Quest Info,Welcome to Ravenhearst,,,,,

starterkit,Quest,Quest Info,"Read This First",,,,,
starterkit_offer,Quest,Quest Info,"The week before the outbreak began, the hospitals of Ravenhearst were bombarded with overdoses thanks to a new designer drug manufactured by the Makasin Cartel. Their towers were the first to be locked down. The Outlaw Bikers MC, screwed over by the Makasin, laid siege to their headquarters. \n\nThe government called in the military to take control and failed, all hell broke loose.",,,,,
starterkit_description,Quest,Quest Info,"Ravenhearst Has Fallen",,,,,
starterkit_subtitle,Quest,Quest Info,The Outbreak Begins,,,,,

magazineClassBlunt,items,Book,Magazine: Blunt Weapons Class Skills,,,,,
magazineClassBrawler,items,Book,Magazine: Brawler Weapons Class Skills,,,,,
magazineClassArchery,items,Book,Magazine: Archery Weapons Class Skills,,,,,
magazineClassPistols,items,Book,Magazine: Pistol Weapons Class Skills,,,,,
magazineClassShotguns,items,Book,Magazine: Shotgun Weapons Class Skills,,,,,
magazineClassRifles,items,Book,Magazine: Rifle Weapons Class Skills,,,,,
magazineClassAutomatics,items,Book,Magazine: Automatic Weapons Class Skills,,,,,
magazineClassJavelin,items,Book,Magazine: Javelin Weapons Class Skills,,,,,
magazineClassBladed,items,Book,Magazine: Bladed Weapons Class Skills,,,,,
magazineClassSledgehammer,items,Book,Magazine: Sledgehammer Weapons Class Skills,,,,,

magazineClassBluntDesc,items,Book,Grants you 15 levels of the Action Skill Blunt Weapons.,,,,,
magazineClassJavelinDesc,items,Book,Grants you 15 levels of the Action Skill Javelin Weapons.,,,,,
magazineClassArcheryDesc,items,Book,Grants you 15 levels of the Action Skill Archery Weapons.,,,,,
magazineClassPistolsDesc,items,Book,Grants you 15 levels of the Action Skill Pistol Weapons.,,,,,
magazineClassShotgunsDesc,items,Book,Grants you 15 levels of the Action Skill Shotgun Weapons.,,,,,
magazineClassRiflesDesc,items,Book,Grants you 15 levels of the Action Skill Rifle Weapons.,,,,,
magazineClassAutomaticsDesc,items,Book,Grants you 15 levels of the Action Skill Automatic Weapons.,,,,,
magazineClassBrawlerDesc,items,Book,Grants you 15 levels of the Action Skill Brawler Weapons.,,,,,
magazineClassBladedDesc,items,Book,Grants you 15 levels of the Action Skill Bladed Weapons.,,,,,
magazineClassSledgehammerDesc,items,Book,Grants you 15 levels of the Action Skill Sledgehammer Weapons.,,,,,


qPistolClass,items,Quest - Note,Pistol Class,,,,,
qPistolClassDesc,items,Quest - Note,Read this book to begin your training in pistols. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qShotgunClass,items,Quest - Note,Shotgun Class,,,,,
qShotgunClassDesc,items,Quest - Note,Read this book to begin your training in shotguns. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qRifleClass,items,Quest - Note,Rifle Class,,,,,
qRifleClassDesc,items,Quest - Note,Read this book to begin your training in rifles. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qAutomaticsClass,items,Quest - Note,Automatics Class,,,,,
qAutomaticsClassDesc,items,Quest - Note,Read this book to begin your training in automatic weapons. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qArcheryClass,items,Quest - Note,Archer Class,,,,,
qArcheryClassDesc,items,Quest - Note,Read this book to begin your training in bows and crossbows. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qBluntClass,items,Quest - Note,Blunt Class,,,,,
qBluntClassDesc,items,Quest - Note,Read this book to begin your training in blunt weapons. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qBladeClass,items,Quest - Note,Blade Class,,,,,
qBladeClassDesc,items,Quest - Note,Read this book to begin your training in bladed weapons. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qBrawlerClass,items,Quest - Note,Brawler Class,,,,,
qBrawlerClassDesc,items,Quest - Note,Read this book to begin your training in fist weapons. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qSledgehammerClass,items,Quest - Note,Sledgehammer Class,,,,,
qSledgehammerClassDesc,items,Quest - Note,Read this book to begin your training in fist weapons. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qJavelinClass,items,Quest - Note,Javelin Class,,,,,
qJavelinClassDesc,items,Quest - Note,Read this book to begin your training in javelin weapons. When you have completed training you will receive a schematic that will teach you new craftable weapons for this class that will be exclusive to you.,,,,,

qHardcoreClass,items,Quest - Note,Hardcore Class,,,,,
qHardcoreClassDesc,items,Quest - Note,Read this book to begin with nothing. Surviving is your proving grounds.,,,,,


q_PistolClass0a,Quest,Quest Info,Pistol Training,,,,,

q_PistolClass,Quest,Quest Info,Pistol Training,,,,,

q_Class0a,Quest,Quest Info,"Beginning Your Training",,,,,
q_Class0a_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. You were good with this weapon before but you will need to be even better now. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_Class0a_description,Quest,Quest Info,"You have been given 15 levels of your specialized skill.",,,,,
q_Class0a_subtitle,Quest,Quest Info,"Training",,,,,


q_PistolClass0,Quest,Quest Info,"Using Your Pistol",,,,,
q_PistolClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_PistolClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_PistolClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_PistolClass1,Quest,Quest Info,"Using Your Pistol",,,,,
q_PistolClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_PistolClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_PistolClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_PistolClass2,Quest,Quest Info,"Using Your Pistol",,,,,
q_PistolClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_PistolClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_PistolClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ShotgunClass0a,Quest,Quest Info,Shotgun Training,,,,,

q_ShotgunClass,Quest,Quest Info,Shotgun Training,,,,,

q_ShotgunClass0,Quest,Quest Info,"Using Your Shotgun",,,,,
q_ShotgunClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ShotgunClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ShotgunClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ShotgunClass1,Quest,Quest Info,"Using Your Shotgun",,,,,
q_ShotgunClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ShotgunClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ShotgunClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ShotgunClass2,Quest,Quest Info,"Using Your Shotgun",,,,,
q_ShotgunClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ShotgunClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ShotgunClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,

q_RifleClass0a,Quest,Quest Info,Rifle Training,,,,,

q_RifleClass,Quest,Quest Info,Rifle Training,,,,,

q_RifleClass0,Quest,Quest Info,"Using Your Rifle",,,,,
q_RifleClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_RifleClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_RifleClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_RifleClass1,Quest,Quest Info,"Using Your Rifle",,,,,
q_RifleClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_RifleClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_RifleClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_RifleClass2,Quest,Quest Info,"Using Your Rifle",,,,,
q_RifleClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_RifleClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_RifleClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,

q_AutomaticsClass0a,Quest,Quest Info,Automatic Weapon Training,,,,,

q_AutomaticsClass,Quest,Quest Info,Automatic Weapon Training,,,,,

q_AutomaticsClass0,Quest,Quest Info,"Using Your Automatic",,,,,
q_AutomaticsClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_AutomaticsClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_AutomaticsClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_AutomaticsClass1,Quest,Quest Info,"Using Your Automatic",,,,,
q_AutomaticsClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_AutomaticsClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_AutomaticsClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_AutomaticsClass2,Quest,Quest Info,"Using Your Automatic",,,,,
q_AutomaticsClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_AutomaticsClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_AutomaticsClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ArcheryClass0a,Quest,Quest Info,Archery Training,,,,,

q_ArcheryClass,Quest,Quest Info,Archery Training,,,,,

q_ArcheryClass0,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ArcheryClass1,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ArcheryClass2,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ArcheryClass3,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass3_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass3_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass3_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_ArcheryClass4,Quest,Quest Info,"Using Your Archery",,,,,
q_ArcheryClass4_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_ArcheryClass4_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_ArcheryClass4_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



q_BluntClass0a,Quest,Quest Info,Blunt Weapons Training,,,,,

q_BluntClass,Quest,Quest Info,Blunt Weapons Training,,,,,

q_BluntClass0,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BluntClass1,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BluntClass2,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BluntClass3,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass3_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass3_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass3_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BluntClass4,Quest,Quest Info,"Using Your Blunt Weapon",,,,,
q_BluntClass4_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BluntClass4_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BluntClass4_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



q_BladeClass0a,Quest,Quest Info,Bladed Weapons Training,,,,,

q_BladeClass,Quest,Quest Info,Bladed Weapons Training,,,,,

q_BladeClass0,Quest,Quest Info,"Using Your Blade Weapon",,,,,
q_BladeClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BladeClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BladeClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BladeClass1,Quest,Quest Info,"Using Your Blade Weapon",,,,,
q_BladeClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BladeClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BladeClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BladeClass2,Quest,Quest Info,"Using Your Blade Weapon",,,,,
q_BladeClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BladeClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BladeClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



q_BladeClass3,Quest,Quest Info,"Using Your Blade Weapon",,,,,
q_BladeClass3_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BladeClass3_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BladeClass3_subtitle,Quest,Quest Info,"Kill Zombies",,,,,

q_JavelinClass0a,Quest,Quest Info,Javelin Weapons Training,,,,,

q_JavelinClass,Quest,Quest Info,Javelin Weapons Training,,,,,

q_JavelinClass0,Quest,Quest Info,"Using Your Javelin Weapon",,,,,
q_JavelinClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_JavelinClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_JavelinClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_JavelinClass1,Quest,Quest Info,"Using Your Javelin Weapon",,,,,
q_JavelinClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_JavelinClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_JavelinClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_JavelinClass2,Quest,Quest Info,"Using Your Javelin Weapon",,,,,
q_JavelinClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_JavelinClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_JavelinClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



q_JavelinClass3,Quest,Quest Info,"Using Your Javelin Weapon",,,,,
q_JavelinClass3_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_JavelinClass3_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_JavelinClass3_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BrawlerClass0a,Quest,Quest Info,Brawler Weapons Training,,,,,

q_BrawlerClass,Quest,Quest Info,Brawler Weapons Training,,,,,

q_BrawlerClass0,Quest,Quest Info,"Using Your Brawler Weapon",,,,,
q_BrawlerClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BrawlerClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BrawlerClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BrawlerClass1,Quest,Quest Info,"Using Your Brawler Weapon",,,,,
q_BrawlerClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BrawlerClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BrawlerClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_BrawlerClass2,Quest,Quest Info,"Using Your Brawler Weapon",,,,,
q_BrawlerClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BrawlerClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BrawlerClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



q_BrawlerClass3,Quest,Quest Info,"Using Your Brawler Weapon",,,,,
q_BrawlerClass3_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_BrawlerClass3_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_BrawlerClass3_subtitle,Quest,Quest Info,"Kill Zombies",,,,,

q_SledgehammerClass0a,Quest,Quest Info,Sledgehammer Weapons Training,,,,,

q_SledgehammerClass,Quest,Quest Info,Sledgehammer Weapons Training,,,,,

q_SledgehammerClass0,Quest,Quest Info,"Using Your Sledgehammer Weapon",,,,,
q_SledgehammerClass0_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_SledgehammerClass0_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_SledgehammerClass0_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_SledgehammerClass1,Quest,Quest Info,"Using Your Sledgehammer Weapon",,,,,
q_SledgehammerClass1_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_SledgehammerClass1_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_SledgehammerClass1_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


q_SledgehammerClass2,Quest,Quest Info,"Using Your Sledgehammer Weapon",,,,,
q_SledgehammerClass2_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_SledgehammerClass2_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_SledgehammerClass2_subtitle,Quest,Quest Info,"Kill Zombies",,,,,



q_SledgehammerClass3,Quest,Quest Info,"Using Your Sledgehammer Weapon",,,,,
q_SledgehammerClass3_offer,Quest,Quest Info,"In order to survive the apocalypse you will need to become familiar with your weapon of choice. Get out there and kill some zombies. When I feel like your training is complete I will reward you with a book that will teach you some exclusive weapons that can be used to survive.",,,,,
q_SledgehammerClass3_description,Quest,Quest Info,"Use the required weapon listed below to kill some zombies. Progressing to the end of this quest line will give you access to further weapons in your specialty.",,,,,
q_SledgehammerClass3_subtitle,Quest,Quest Info,"Kill Zombies",,,,,


qChefClass,items,Quest - Note,Chef Class,,,,,
qChefClassDesc,items,Quest - Note,"You were a chef in your former life before all of this. Reading this will give you a Cooking Grill, a Cooking Pot, 2 Chili Dogs and 3 Berry Teas along with schematics for both.",,,,,

qMinerClass,items,Quest - Note,Miner Class,,,,,
qMinerClassDesc,items,Quest - Note,"You were a trade worker in your former life before all of this. Reading this will give you a Pickaxe and schematics to craft a Pickaxe.",,,,,

qLumberjackClass,items,Quest - Note,Lumberjack Class,,,,,
qLumberjackClassDesc,items,Quest - Note,"You were a trade worker in your former life before all of this. Reading this will give you a Fireaxe and schematics to craft a Fireaxe.",,,,,

qExplosivesClass,items,Quest - Note,Explosives Class,,,,,
qExplosivesClassDesc,items,Quest - Note,"You were a chef in your former life before all of this. Reading this will give you a Cooking Grill, a Cooking Pot, 2 Chili Dogs and 3 Berry Teas along with schematics for both.",,,,,

qMedicClass,items,Quest - Note,Medic Class,,,,,
qMedicClassDesc,items,Quest - Note,"You were a paramedic in your former life before all of this. Reading this will give you 2 Painkillers, 1 Splint, 2 Herbal Antibiotics and schematics for the Herbal Antibiotic and First Aid Bandages.",,,,,

qFarmerClass,items,Quest - Note,Farmer Class,,,,,
qFarmerClassDesc,items,Quest - Note,"You were a farmer in your former life before all of this. Reading this will give you a Hoe, 2 Seed Packs, 5 Compost and a Farm Table.",,,,,

qHardcoreClass,items,Quest - Note,Hardcore Class,,,,,
qHardcoreClassDesc,items,Quest - Note,"You were a streamer in your former life. Good luck. Kappa. This means you start with nothing and your journey is much more difficult than others.",,,,,


q_ChefClass0a,Quest,Quest Info,Chef Class,,,,,

q_MinerClass0a,Quest,Quest Info,Miner Class,,,,,

q_LumberjackClass0a,Quest,Quest Info,Lumberjack Class,,,,,

q_ExplosivesClass0a,Quest,Quest Info,Explosives Class,,,,,

q_MedicClass0a,Quest,Quest Info,Medic Class,,,,,

q_FarmerClass0a,Quest,Quest Info,Farmer Class,,,,,

q_HardcoreClass0a,Quest,Quest Info,Hardcore Class,,,,,



q_ChefClass,Quest,Quest Info,Chef Class,,,,,

q_MinerClass,Quest,Quest Info,Miner Class,,,,,

q_LumberjackClass,Quest,Quest Info,Lumberjack Class,,,,,

q_ExplosivesClass,Quest,Quest Info,Explosives Class,,,,,

q_MedicClass,Quest,Quest Info,Medic Class,,,,,

q_FarmerClass,Quest,Quest Info,Farmer Class,,,,,

q_HardcoreClass,Quest,Quest Info,Hardcore Class,,,,,



q_HardcoreClass0a_description,Quest,Quest Info,"You set out to prove that you can tackle the apocalypse with help from no one. Your only set of skills are your eyes and ears.",,,,,

q_Class0b_description,Quest,Quest Info,"Check your inventory. You have been given some items and schematics pertaining to your profession.",,,,,
q_Class0b_subtitle,Quest,Quest Info,"Specialization",,,,,
q_Class0b_offer,Quest,Quest Info,"The skills that earned you money in your previous life will help you survive in this one. Use your former knowledge to gain an advantage in the Apocalypse.",,,,,

q_Class0c_offer,Quest,Quest Info,"You set out to prove that you can tackle the apocalypse with help from no one. Your only set of skills are your eyes and ears.",,,,,

resourceCompletedMeleeChaptersRH,Items,Item,Completed Melee Training,,,,,
resourceCompletedMeleeChaptersRHDesc,Items,Item,This is a completed melee certificate. Hold on to this. It will be needed to complete the final step of the journal questline.,,,,,

resourceCompletedRangedChaptersRH,Items,Item,Completed Ranged Training,,,,,
resourceCompletedRangedChaptersRHDesc,Items,Item,This is a completed ranged certificate. Hold on to this. It will be needed to complete the final step of the journal questline.,,,,,


meleeToolAxeT1IronFireaxeSchematic,items,Tool,Iron Fireaxe Schematic,,,,,
meleeToolShovelT1IronShovelSchematic,items,Tool,Iron Shovel Schematic,,,,,
meleeToolPickT1IronPickaxeSchematic,items,Tool,Iron Pickaxe Schematic,,,,,
meleeToolRepairT1ClawHammerSchematic,items,Tool,Claw Hammer Schematic,,,,,
meleeToolAxeT2SteelAxeSchematic,items,Tool,Steel Fireaxe Schematic,,,,,
meleeToolShovelT2SteelShovelSchematic,items,Tool,Steel Shovel Schematic,,,,,
meleeToolPickT2SteelPickaxeSchematic,items,Tool,Steel Pickaxe Schematic,,,,,