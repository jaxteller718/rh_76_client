
Addition: Our own Ravenhearst Launcher which will handle patches, updates and installs.

Addition: Valmars Foods Modlet

Addition: Food Spoilage (See Read Me For Full Details)

Addition: Better Ramps for Vehicles

Addition: Mining Machine by StallionsDen

Addition: Fertilized Seeds. Right click planted seeds with fertilizer in hand to upgrade to Fertilized Seed. They will yield more crops and also when punched will downgrade to a normal version of the seed you can fertilize again.

Addition: RH 5 Foods

Addition: New Loading Screen Tips

Addition: New Journal Quests Explaining RH Mechanics

Addition: Appliance Table

Addition: Food Prep Table

Addition: Oil Refinery

Addition: 3 new trader quests

Addition: Military Backpack crafted from 10 backpacks that unlock last row of slots after you have unlocked the default RH amount.

Addition: Lathe

Addition: Advanced Turrets Modlet adds 3 new turrets

Addition: Jax Mastery Modlet that extends perks in all areas. These are unlocked after Level 10 of Skills and Level 100 of Action Skills.

Addition: Toads Vehicle Mods found in loot and trader

Addition: Ravenhearst Food and Resources to trader

Addition: High Powered Engine crafted only for late stage vehicles

Addition: Honey and Shiv steps to open quest

Addition: Additional Motorcycles in T5

Addition: Better Book Icons

Addition: Bandages are now dirty and need sanitized

Addition: King Gen Maps

Addition: New Trader Jobs

Addition: Invisible Armor Dye Modlet

Addition: New Phone Quest

Addition: NPC and Traders

Addition: Over 10 New Bdubya Vehicles

Addition: Lathes added to a very few late game pois

Addition: Armor UI Added by Redbeardt. Will now show a display on screen when armor is going to break. 

Addition: Twitch Modlets to kill Fox with

Addition: Hazmat Gears now show armor values

Addition: 4 Potted Plants

Addition: Electrician Table for electrical devices such as new lights and signs and speakers.

Addition: Ammunition Table added to select POIs

Addition: Actual Ore Added to World for all of our ores

Addition: Containers to slow down the spoilage process




Balance: Buffs for sprains are changed to 5 minutes and the broken, concussion, and laceration are changed to 30 minutes

Balance: Created Tier 0 Zombies with less health than vanilla. Tier 0 will last until gamestage 5 and will be reduced spawns. This will provide a relief on Day 1 spawns and hopefully make for a smoother opening transition to our zombies post GS5

Balance: Readjusted All Vehicle Healths

Balance: Adjusted Tool Craft Timers

Balance: Extended Phone Quest Respawn Times

Balance: Reduced amount of meat steel knuckles harvest

Balance: Turret Overhaul. Less damage, less distance and perk reductions

Balance: Reduced Craft time on PWB

Balance: Slot Balances on all storages




Change: Table Saw now unlocked via quest

Change: Table Saw Now Requires Fuel

Change: Thumper Event completely reworked by VvRaynevV

Change: New Fishing System

Change: Opening Quest Rearranged

Change: Farming Journal Updated

Change: Sugar now made from Honeycomb

Change: Storage can no longer be picked up

Change: Expanded Loading Screen Tips by foxcheckman

Change: Weapon class quests now give a completed certificate to use in the Journal Completion Recipe

Change: Oil Refinery and Mining Machine Now Journal Completion Rewards

Change: Ammo Ingredients now given as reward for completing gun quests

Change: Added RH weapons to class quest lines

Change: Lessened Fog

Change: Lightened Moon Light

Change: Lightened Nights

Change: Removed weapon quests from starting quest and added read class paper step

Change: Integrated Parkour into Athletics

Change: Bone Fragments added to birds nest destroy

Change: New Potted Plants to random plant spawner in houses

Change: Riles UI Now Default. You can still change to other uis via menu




Fix: Discrepencies in journal and tips for Irrigation

Fix: Reach on Behemoth Lessened

Fix: Removed Bad POI Name

Fix: Pallets Breaking Back to Iron pallets

Fix: Improper Class Quest orders

Fix: Tree Sap Scrapping

Fix: Redbeardts Linux Fixes for Active Ingredients

Fix: Fall Damage




Moved: Boating Modlet

Moved: Vehicle Paint Job

Moved: CCTV

Moved: Unnecessary But Beautiful




Removed: Skins for Tiered Zombies (Performance Heavy)

Removed: Telric Pois

Removed: Duster

Removed: Maybell's Ranch and Horses

Removed: Fpshud due to not being updated




**Code Changes**

The following are code changes done by W00kieN00kie


Addition: Reset Progression now doesnt reset the Action Skills and Pack Mule levels when using the Forgetting Elixir. The Action Skill Crafting are still reset though as they are just skill points and hence can be relearned with points.




2 Ravenhearst 6k pregenned Maps and 2 8k Ravenhearst pregenned maps made by myself


RH Navezgane

Removed: Maybell's Ranch

Added: 17 Additional Pois




**Newly Added Pois**

All of the following pois here have been checked, deco added, adjusted footprints, sleepers sweep, loot overhaul etc by Sinder and are new to Ravenhearst


POI: Added rh_medieval_Alchemist(by_VitaminE)

POI: Added rh_House_Burrowsby_Nauti_Angel)

POI: Added rh_govt_CDC_Mission(by_TheFootpad)

POI: Added rh_mili_Quarantine_Bunker(by_NLBEagle)

POI: Added rh_mili_flight_tower(by_NLBEagle)

POI: Added rh_mili_FuelStation(by_NLBEagle)

POI: Added rh_HBW_MetroStation(by_Hydro)

POI: Added rh_school_MidwichElementary(by_Dragoness)

POI: Added rh_mili_Quarantine_Post(by_NLBEagle)

POI: Added rh_industry_PowerPlant_Kam(redoBy_sinder)

POI: Added rh_attr_SkiResort_Bar(by_NLBEagle)

POI: Added rh_food_Small_Cafe(by_Bostonlondon)

POI: Added rh_govt_Pentagon(by_Nauti_Angel)

POI: Added rh_mansion_serial_killer(by_FLESHUS)

POI: Added rh_medieval_house_pigeon(by_VitaminE)

POI: Added rh_medieval_business_Bakery(by_VitaminE)

POI: Added rh_medieval_Tower(by_VitaminE)

POI: Added rh_transit_BusStation(by_Dragoness)

POI: Added rh_warehouse(by_NLBEagle)

POI: Added rh_food_KFZ_001(by_Evilracc0on)

POI: Added rh_food_KFZ_002(by_Evilracc0on)

POI: Added rh_settlement_TWD_Woodbury(by_SurvivorAndy)

POI: Added rh_trailer_001(by_Evilracc0on)

POI: Added rh_trailer_002(by_Evilracc0on)

POI: Added rh_trailer_003(by_Evilracc0on)

POI: Added rh_trailer_park(by_Evilracc0on)

POI: Added rh_business_strip_old_10(by_Deverezieaux)

POI: Added rh_food_KFZ_003(by_Evilracc0on)

POI: Added rh_Hill_House(by_Captain_and_Mental)

POI: Added rh_House_At_The_Lake(by_Rukminesh)

POI: Added rh_house_duplex_old_01_Blue(by_Deverezieaux)

POI: Added rh_house_duplex_old_03_brown(by_Deverezieaux)

POI: Added rh_house_duplex_old_05_burned(by_Deverezieaux)

POI: Added rh_house_LB_003(by_EvilRacc0on)

POI: Added rh_industry_Oil_Well(by_Rukminesh)

POI: Added rh_mansion_House_OfThe_Forest(by_Rukminesh)

POI: Added rh_store_general(by_mexico664)

POI: Added rh_house_DaveHome2(by_DMC)

POI: Added rh_Eden_Center_Mall(by_DOD)

POI: Added rh_medieval_Jarls_Estate(by_Eihwaz)

POI: Added rh_superstore_Tesko_Supermarket(by_SurvivorAndy)

POI: Added rh_Army_Camp_Heli_Base_01A(by_NLBEagle_KrunchEdit)

POI: Added rh_Asia_Pagoda_sm_01(by_magoli_KrunchEdit)

POI: Added rh_apts_Styles_Apartment_Complex(by_JSDoctor_KrunchEdit)

POI: Added rh_Army_Garage(by_NLBEagle_KrunchEdit)

POI: Added rh_Army_Camp_HQ_01A(by_NLBEagle_KrunchEdit)

POI: Added rh_business_Corner_Diner(by_NLBEagle_KrunchEdit)

POI: Added rh_business_Corner_Gunstore(by_NLBEagle_KrunchEdit)

POI: Added rh_business_Corner_Pizza(by_NLBEagle_KrunchEdit)

POI: Added rh_store_Bite_Aid_Pharmacy_01(by_zztong)

POI: Added rh_school_beauty_01(by_Deverezieaux)

POI: Added rh_skyscraper_government_01(by_Deverezieaux)

POI: Added rh_Bar_Pool_Hall_01(by_zztong)

POI: Added rh_house_Brownstones_01(by_zztong)

POI: Added rh_house_Cabin_Fort_01(by_zztong)

POI: Added rh_attr_Drive_In_Movie_Theater_01(by_zztong)

POI: Added rh_food_store_Drive_Thru_01(by_zztong)

POI: Added rh_medical_EMS_01(by_zztong)

POI: Added rh_attr_wild_Famers_Market_01(by_zztong)

POI: Added rh_House_01(by_zztong)

POI: Added rh_House_02(by_zztong)

POI: Added rh_business_KZMB_Radio(by_zztong)

POI: Added rh_business_Law_Offices_01(by_zztong)

POI: Added rh_business_Propane_01(by_zztong)

POI: Added rh_Trailer_Park_01(by_zztong)

POI: Added rh_industry_Lumber_Yard_01(by_zztong)

POI: Added rh_industry_Masonry_Yard_01(by_zztong)

POI: Added rh_business_Office_Bldg_01(by_zztong)

POI: Added rh_Church_L4D_DT(by_CraterCreator)

POI: Added rh_house_cabin_L4D_DT(by_CraterCreator)

POI: Added rh_wild_industry_Tower_L4D_DT(by_CraterCreator)

POI: Added rh_industry_outpost_rust(by_cjens)

POI: Added rh_medical_Doc_Jen_Clinic(by_Evilracc0on)

POI: Added rh_house_abandoned_11(by_Deverezieaux)

POI: Added rh_house_03(by_zztong)

POI: Added rh_food_Restaurant_01(by_zztong)

POI: Added rh_z57_Army_04_Silo(by_Zyncosa)

POI: Added rh_z53_House_14(by_Zyncosa)

POI: Added rh_house(by_Yakov)




**POI Fixes by Sinder**


POI: Military Tunnels - Removed garbage deco causing truck to float. reworked sleeper volumes to not dupe spawns.unused sleeper blocks now covered by volumes. added some sleepers to compensate loss of dupe spawns. changed some volumes to attack. small loot additions.

POI: Junkyard - Fixed glitchy sleeper, shuffled deco and loot.

POI: Little League Baseball Shelter - Removed modded cars. Increased zombies a touch.

POI: Vet Clinic - Removed modded cars, redid sleepers a bit

POI: Museum of Fear - Removed old blue car from exhibit, replaced with broken glass, 1 tire, and a motor.

POI: Bowling Center Mesh Redone

POI: No Hope Mall - Removed 2 "Ravenhearst car spawners"

POI: Walcart- Removed offending fetch container, but moved all other fetches to new locations. upped sleepers a bit.

POI: Pre-trader - Removed trader quest, linked some of the volumes so spawning might be better. fixed two volumes from over-calling sleeper amounts.

POI: Walking Dead Prison - Removed some trees from border to better see prison and rad smoke. added 4 more smoke locations to perimeter so more obvious.

POI: Trailer Park - Fixed air slice on side of poi

POI: Removed 2 Passive volumes from Midwich

POI: Removed missing rugs from Museum

POI: Moved Stuck Sleeper in Army Pois

POI: All tier 5 pois reviewed to fix volumes from over-calling sleeper amounts

POI: Added proper "showquestclear" counts to all tier 5 pois. Will work on adding these to all pois over time.

